const { user, produk, transaksi, pembayaran } = require("../db/models");

produk.hasMany(transaksi, {foreignKey: "produk_id"})
transaksi.belongsTo(produk, {foreignKey: "produk_id"})

user.hasMany(transaksi, { foreignKey: "user_id" });
transaksi.belongsTo(user, { foreignKey: "user_id" });

transaksi.hasMany(pembayaran, {foreignKey: "order_id"})
pembayaran.belongsTo(transaksi, {foreignKey: "order_id"})
