const { produk } = require("../db/models");

class ProdukController {
  async createProduk(req, res) {
    try {
      let createdData = await produk.create({
        name: req.body.name,
        price: req.body.price,
        qty: req.body.qty,
      });

      let data = await produk.findOne({
        where: {
          id: createdData.id,
        },
        attributes: ["id", "name", "price", "qty", "createdAt", "updatedAt"],
      });

      return res.status(201).json({
        message: "Success",
        data,
      });
    } catch (e) {
    
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  async getOneProduk(req, res) {
    try {
      let data = await produk.findOne({
        where: { id: req.params.id },
        attributes: [
          "id",
          "name",
          "price",
          "qty",
          ["createdAt", "createdAt"],
        ],
      });
      if (!data) {
        return res.status(404).json({
          message: "Produk Not Found",
        });
      }

      return res.status(201).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  async getAllProduk(req, res) {
    try {
      let data = await produk.findAll({
        attributes: [
          "id",
          "name",
          "price",
          "qty",
          ["createdAt", "createdAt"],
        ],
      });

      if (data.length === 0) {
        return res.status(404).json({
          message: "Produk Not Found",
        });
      }
      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  async updateProduk(req, res) {
    let update = {
      name: req.body.name,
      price: req.body.price,
      qty: req.body.qty,
    };
    try {
      let data = await produk.update(update, {
        where: { id: req.params.id },
        attributes: ["name", "price", "qty"],
      });
      const name = update.name;
      const price = update.price;
      const qty = update.qty;

      return res.status(201).json({
        message: "Update Produk Success",
        name,
        price,
        qty,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
  async deleteProduk(req, res) {
    try {
      let data = await produk.destroy({
        where: { id: req.params.id },
      });

      return res.status(201).json({
        message: "Delete Produk Success",
      });
    } catch (e) {
      // If error
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
}

module.exports = new ProdukController();
