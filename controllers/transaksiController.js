const { transaksi, produk, user } = require("../db/models");

class TransaksiController {
  async getAll(req, res) {
    try {
      let data = await transaksi.findAll({
        where: { user_id: req.user.id },

        attributes: ["order_id", "amount", "status", ["createdAt", "waktu"]],
        include: [
          {
            model: produk,
            attributes: ["name", "price", "qty"],
          },
          {
            model: user,
            attributes: ["name"],
          },
        ],
      });

      if (data.length == 0) {
        return res.status(400).json({
          message: "You don't have any transaction",
        });
      } else {
        return res.status(200).json({
          message: "Success",
          data,
        });
      }
    } catch (e) {
      
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
  async getOne(req, res) {
    try {
      let data = await transaksi.findOne({
        where: { user_id: req.user.id, order_id: req.params.id },

        attributes: ["order_id", "amount", "status", ["createdAt", "waktu"]],
        include: [
          {
            model: produk,
            attributes: ["name", "price", "qty"],
          },
          {
            model: user,
            attributes: ["name"],
          },
        ],
      });

      if (!data) {
        return res.status(404).json({
          message: "Transaksi Not Found",
        });
      }

      return res.status(200).json({
        message: "Success",
        data: data,
      });
    } catch (e) {
      
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  async create(req, res) {
    try {
      let findData = await Promise.all([
        produk.findOne({ where: { id: req.body.produk_id } }),
        user.findOne({ where: { id: req.user.id } }),
      ]);

      let errors = [];
     

      if (!findData[0]) {
        errors.push("Produk Not Found");
      }

      if (!findData[1]) {
        errors.push("User Not Found");
      }

      if (errors.length > 0) {
        return res.status(404).json({
          message: errors.join(", "),
        });
      }

      let price = findData[0].price;
      let amount = price;

      let createdData = await transaksi.create({
        user_id: req.user.id,
        produk_id: req.body.produk_id,
        price,
        amount,
      });
      let data = await transaksi.findOne({
        where: { order_id: createdData.order_id },
        attributes: ["order_id", "amount", "status", ["createdAt", "waktu"]],
        include: [
          {
            model: produk,
            attributes: ["name", "price"],
          },
          {
            model: user,
            attributes: ["name"],
          },
        ],
      });

      return res.status(201).json({
        message: "Success",
        data,
      });
    } catch (e) {
     
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
}

module.exports = new TransaksiController();
