const request = require("supertest");
const app = require("../index");
const { produk, user, transaksi } = require("../db/models");
let tokenCustomer;

//////////////////////////////////////////////////////////////// User Sign Up / Sign In //////////////////////////////////////////////////////////////////
// Auth test
describe("Auth Test", () => {
  describe("/signup POST", () => {
    it("It should make user and get the token", async () => {
      const res = await request(app).post("/auth/signup").send({
        name: "Alfian1234",
        email: "alfian1234@gmail.com",
        password: "Alfian_1234",
        confirmPassword: "Alfian_1234",
      });
      expect(res.statusCode).toEqual(200);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body.message).toEqual("Success");
    });
  });
  describe("/signup POST", () => {
    it("It should error email has been used", async () => {
      const res = await request(app).post("/auth/signup").send({
        name: "Alfian1234",
        email: "alfian1234@gmail.com",
        password: "Alfian_1234",
        confirmPassword: "Alfian_1234",
      });
      expect(res.statusCode).toEqual(401);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body.message).toEqual("Email has been used");
    });
  });

  describe("/signup POST", () => {
    it("It should error confirm password", async () => {
      const res = await request(app).post("/auth/signup").send({
        name: "Alfian1234",
        email: "alfian1234@gmail.com",
        password: "Alfian_1234",
        confirmPassword: "Alfian_12345",
      });
      expect(res.statusCode).toEqual(400);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body.message).toEqual(
        "Password confirmation must be same as password"
      );
    });
  });
  describe("/signup POST", () => {
    it("It should error email format", async () => {
      const res = await request(app).post("/auth/signup").send({
        name: "Alfian1234",
        email: "alfian1234com",
        password: "Alfian_1234",
        confirmPassword: "Alfian_1234",
      });
      expect(res.statusCode).toEqual(400);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body.message).toEqual("Email field must be valid email");
    });
  });
  describe("/signup POST", () => {
    it("It should error weak password", async () => {
      const res = await request(app).post("/auth/signup").send({
        name: "Alfian1234",
        email: "alfian1234@gmail.com",
        password: "1234",
        confirmPassword: "1234",
      });
      expect(res.statusCode).toEqual(400);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body.message).toEqual(
        "Password needs (uppercase & lowercase characters, number, and symbol)"
      );
    });
  });
  describe("/signin POST", () => {
    it("It should success signin", async () => {
      const res = await request(app).post("/auth/signin").send({
        email: "alfian1234@gmail.com",
        password: "Alfian_1234",
      });
      expect(res.statusCode).toEqual(200);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body.message).toEqual("Success");
    });
  });
  describe("/signin POST", () => {
    it("It should error signin email not found", async () => {
      const res = await request(app).post("/auth/signin").send({
        email: "alfian12345@gmail.com",
        password: "Alfian_1234",
      });
      expect(res.statusCode).toEqual(401);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body.message).toEqual("Email not found");
    });
  });
  describe("/signin POST", () => {
    it("It should error signin wrong password", async () => {
      const res = await request(app).post("/auth/signin").send({
        email: "alfian1234@gmail.com",
        password: "Alfian_12345",
      });
      expect(res.statusCode).toEqual(401);
      expect(res.body).toBeInstanceOf(Object);
      expect(res.body.message).toEqual("Wrong password");
    });
  });
});

//////////////////////////////////////////////////////////////// CRUD Produk /////////////////////////////////////
//Create Produk
describe("Create Produk Test", () => {
  describe("produk/create POST", () => {
    it("It should success", async () => {
      if (user.role == "admin") {
        const res = await request(app)
          .post("/produk/create/")
          .send({
            name: "powerbank",
            price: 150000,
            qty: 5,
          })
          .set({
            Authorization: `Bearer ${tokenCustomer}`,
          });

        expect(res.statusCode).toEqual(200);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Success");
      }
    });
  });
  describe("produk/create POST", () => {
    it("It should error price must be a number", async () => {
      if (user.role == "admin") {
        const res = await request(app)
          .post("/produk/create/")
          .send({
            name: "powerbank",
            price: "150000abc",
            qty: 5,
          })
          .set({
            Authorization: `Bearer ${tokenCustomer}`,
          });

        expect(res.statusCode).toEqual(400);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Price must be a number");
      }
    });
  });
  describe("produk/create POST", () => {
    it("It should error quantity must be a number", async () => {
      if (user.role == "admin") {
        const res = await request(app)
          .post("/produk/create/")
          .send({
            name: "powerbank",
            price: 150000,
            qty: "5abc",
          })
          .set({
            Authorization: `Bearer ${tokenCustomer}`,
          });

        expect(res.statusCode).toEqual(400);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Quantity must be a number");
      }
    });
  });
  describe("produk/create POST", () => {
    it("It should error not authorized", async () => {
      if (user.role == "user") {
        const res = await request(app)
          .post("/produk/create/")
          .send({
            name: "powerbank",
            price: 150000,
            qty: 5,
          })
          .set({
            Authorization: `Bearer ${tokenCustomer}`,
          });

        expect(res.statusCode).toEqual(400);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("You're not authorized");
      }
    });
  });
  describe("produk/create POST", () => {
    it("It should error no auth token", async () => {
      if (user.role == "admin") {
        const res = await request(app).post("/produk/create/").send({
          name: "powerbank",
          price: 150000,
          qty: 5,
        });

        expect(res.statusCode).toEqual(403);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("No auth token");
      }
    });
  });
});

//Get All Produk
describe("Get Produk All Test", () => {
  describe("produk/ GET", () => {
    it("It should success", async () => {
      
        const res = await request(app)
          .get("/produk/all")
         
        expect(res.statusCode).toEqual(200);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Success");
      
    });
  });
});

//Get One Produk
describe("Get One Produk Test", () => {
  describe("produk/:id GET", () => {
    it("It should success", async () => {
      
        const res = await request(app)
          .get("/produk/0ea2167b-66d9-4fd5-8adb-3d9202cfb69f")
          
        expect(res.statusCode).toEqual(201);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Success");
      
    });
  });
  describe("produk/:id GET", () => {
    it("It should error produk not found", async () => {
     
        const res = await request(app)
          .get("/produk/5")
        
        expect(res.statusCode).toEqual(404);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Produk Not Found");
      
    });
  });
});

//Update Produk
describe("Update Produk Test", () => {
  describe("produk/update/:id PATCH", () => {
    it("It should success", async () => {
      if (user.role == "admin") {
        const res = await request(app)
          .patch("/produk/update/d159baa0-5fbc-4e14-b21f-1bada797d332")
          .send({
            name: "Guitar",
            price: 1500000,
            qty: 5,
          })
          .set({
            Authorization: `Bearer ${tokenCustomer}`,
          });

        expect(res.statusCode).toEqual(201);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Update Produk Success");
      }
    });
  });
  describe("produk/update/:id PATCH", () => {
    it("It should error produk not found", async () => {
      if (user.role == "admin") {
        const res = await request(app)
          .patch("/produk/update/6")
          .send({
            name: "Guitar",
            price: 1500000,
            qty: 5,
          })
          .set({
            Authorization: `Bearer ${tokenCustomer}`,
          });

        expect(res.statusCode).toEqual(400);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Produk Not Found");
      }
    });
  });
  describe("produk/update/:id PATCH", () => {
    it("It should error price must be a number", async () => {
      if (user.role == "admin") {
        const res = await request(app)
          .patch("/produk/update/d159baa0-5fbc-4e14-b21f-1bada797d332")
          .send({
            name: "Guitar",
            price: "150000abc",
            qty: 5,
          })
          .set({
            Authorization: `Bearer ${tokenCustomer}`,
          });

        expect(res.statusCode).toEqual(400);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Price must be a number");
      }
    });
  });
  describe("produk/update/:id PATCH", () => {
    it("It should error quantity must be a number", async () => {
      if (user.role == "admin") {
        const res = await request(app)
          .patch("/produk/update/d159baa0-5fbc-4e14-b21f-1bada797d332")
          .send({
            name: "Guitar",
            price: "150000abc",
            qty: 5,
          })
          .set({
            Authorization: `Bearer ${tokenCustomer}`,
          });

        expect(res.statusCode).toEqual(400);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Quantity must be a number");
      }
    });
  });
  describe("produk/update/:id PATCH", () => {
    it("It should error not authorized", async () => {
      if (user.role == "user") {
        const res = await request(app)
          .patch("/produk/update/d159baa0-5fbc-4e14-b21f-1bada797d332")
          .send({
            name: "Bass",
            price: 15000000,
            qty: 5,
          });

        expect(res.statusCode).toEqual(400);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("You're not authorized");
      }
    });
  });
  describe("produk/update/:id PATCH", () => {
    it("It should error no auth token", async () => {
      if (user.role == "admin") {
        const res = await request(app)
          .patch("/produk/update/d159baa0-5fbc-4e14-b21f-1bada797d332")
          .send({
            name: "Bass",
            price: 15000000,
            qty: 5,
          });

        expect(res.statusCode).toEqual(403);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("No auth token");
      }
    });
  });
});

//Delete Produk
describe("Delete Produk Test", () => {
  describe("produk/delete/:id DELETE", () => {
    it("It should success", async () => {
      if (user.role == "admin") {
        const res = await request(app)
          .delete("/produk/delete/0ea2167b-66d9-4fd5-8adb-3d9202cfb69f")
          .set({
            Authorization: `Bearer ${tokenCustomer}`,
          });

        expect(res.statusCode).toEqual(201);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Delete Produk Success");
      }
    });
  });
  describe("produk/delete/:id DELETE", () => {
    it("It should error produk not found", async () => {
      if (user.role == "admin") {
        const res = await request(app)
          .delete("/produk/delete/6")

          .set({
            Authorization: `Bearer ${tokenCustomer}`,
          });

        expect(res.statusCode).toEqual(404);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Produk Not Found");
      }
    });
  });

  describe("produk/delete/:id DELETE", () => {
    it("It should error not authorized", async () => {
      if (user.role == "user") {
        const res = await request(app)
          .delete("/produk/delete/0ea2167b-66d9-4fd5-8adb-3d9202cfb69f")

          .set({
            Authorization: `Bearer ${tokenCustomer}`,
          });

        expect(res.statusCode).toEqual(400);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("You're not authorized");
      }
    });
  });
  describe("produk/delete/:id DELETE", () => {
    it("It should error no auth token", async () => {
      if (user.role == "admin") {
        const res = await request(app).delete(
          "/produk/delete/0ea2167b-66d9-4fd5-8adb-3d9202cfb69f"
        );

        expect(res.statusCode).toEqual(403);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("No auth token");
      }
    });
  });
});

//////////////////////////////////////////////////////////////// Transaksi /////////////////////////////////////
//Create Transaksi
describe("Create Transaki Test", () => {
  describe("transaksi/order CREATE", () => {
    it("It should success", async () => {
      if (user.role == "user") {
        const res = await request(app)
          .post("transaksi/order ")
          .send({
            produk_id: "0ea2167b-66d9-4fd5-8adb-3d9202cfb69f",
          })
          .set({
            Authorization: `Bearer ${tokenCustomer}`,
          });

        expect(res.statusCode).toEqual(201);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Success");
      }
    });
  });
  describe("transaksi/order CREATE", () => {
    it("It should error produk not found", async () => {
      if (user.role == "user") {
        const res = await request(app)
          .post("transaksi/order ")
          .send({
            produk_id: "0ea2167b-66d9-4fd5-8adb-3d9202cfb69f",
          })
          .set({
            Authorization: `Bearer ${tokenCustomer}`,
          });

        expect(res.statusCode).toEqual(404);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Produk Not Found");
      }
    });
  });
  describe("transaksi/order CREATE", () => {
    it("It should error no auth token", async () => {
      if (user.role == "user") {
        const res = await request(app).post("transaksi/order ").send({
          produk_id: "0ea2167b-66d9-4fd5-8adb-3d9202cfb69f",
        });

        expect(res.statusCode).toEqual(404);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("No auth token");
      }
    });
  });
});

//Get All Transaksi
describe("Get Transaksi All Test", () => {
  describe("transaksi/ GET", () => {
    it("It should success", async () => {
      if (user.role == "user") {
        const res = await request(app)
          .get("/transaksi")
          .set({
            Authorization: `Bearer ${tokenCustomer}`,
          });

        expect(res.statusCode).toEqual(201);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Success");
      }
    });
  });
  describe("transaksi/ GET", () => {
    it("It should error no auth token", async () => {
      if (user.role == "user") {
        const res = await request(app).get("/transaksi");

        expect(res.statusCode).toEqual(403);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("No auth token");
      }
    });
  });
});

//Get One Transaksi
describe("Get One Transaksi Test", () => {
  describe("transaksi/:id GET", () => {
    it("It should success", async () => {
      if (user.role == "user") {
        const res = await request(app)
          .get("/transaksi/176eaf7f-a401-46d1-a893-05267cb0c055")
          .set({
            Authorization: `Bearer ${tokenCustomer}`,
          });

        expect(res.statusCode).toEqual(404);
        expect(res.body).toBeInstanceOf(Object);
        expect(res.body.message).toEqual("Success");
      }
    });
  });
  describe("produk/:id GET", () => {
    describe("transaksi/:id GET", () => {
      it("It should success", async () => {
        if (user.role == "user") {
          const res = await request(app)
            .get("/transaksi/176eaf7f-a401-46d1-a893-05267cb0c05")
            .set({
              Authorization: `Bearer ${tokenCustomer}`,
            });

          expect(res.statusCode).toEqual(404);
          expect(res.body).toBeInstanceOf(Object);
          expect(res.body.message).toEqual("Transaksi Not Found");
        }
      });
    });

    describe("produk/:id GET", () => {
      it("It should error no auth token", async () => {
        if (user.role == "user") {
          const res = await request(app).get(
            "/produk/176eaf7f-a401-46d1-a893-05267cb0c055"
          );

          expect(res.statusCode).toEqual(403);
          expect(res.body).toBeInstanceOf(Object);
          expect(res.body.message).toEqual("No auth token");
        }
      });
    });
  });
});
