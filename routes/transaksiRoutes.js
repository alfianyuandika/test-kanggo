const express = require("express");
const router = express.Router();

const transaksiValidator = require("../middlewares/validators/transaksiValidator");

const transaksiController = require("../controllers/transaksiController");

const auth = require("../middlewares/auth");

router.post(
  "/order",
  auth.user,
  transaksiValidator.create,
  transaksiController.create
);

router.get("/", auth.user, transaksiController.getAll);

router.get("/:id", auth.user, transaksiController.getOne);

module.exports = router;
