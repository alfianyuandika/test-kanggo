const express = require("express");
const router = express.Router();

const produk = require("./produkRoutes");
const transaksi = require("./transaksiRoutes");
const auth = require("./authRoutes");
const pembayaran = require("./pembayaranRoute")

router.use("/produk", produk);
router.use("/auth", auth);
router.use("/transaksi", transaksi);
router.use("/pembayaran", pembayaran)


module.exports = router;
