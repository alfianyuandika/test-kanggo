const express = require("express");
const router = express.Router();

const produkValidator = require("../middlewares/validators/produkValidator");

const produkController = require("../controllers/produkController");

const auth = require("../middlewares/auth");


router.get("/all", produkController.getAllProduk);

router.get("/:id", produkController.getOneProduk);

router.post(
  "/create",
  auth.admin,
  produkValidator.create,
  produkController.createProduk
);

router.delete(
  "/delete/:id",
  auth.admin,
  produkValidator.delete,
  produkController.deleteProduk
);

router.patch(
  "/update/:id",
  auth.admin,
  produkValidator.update,
  produkController.updateProduk
);

module.exports = router;
