'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("transaksi", [
      {
        order_id: "176eaf7f-a401-46d1-a893-05267cb0c055",
        user_id: "4adc4e9e-fc6b-4644-b1e6-6b6a1433a9ac",
        produk_id: "0ea2167b-66d9-4fd5-8adb-3d9202cfb69f",
        status: "pending",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        order_id: "264ea404-d820-44e4-b3ab-deea43bcec3b",
        user_id: "cf3efecf-2559-4c3b-b4e3-8d3100c8c931",
        produk_id: "2c425fe2-f62e-4f1a-a2e2-66869b081a92",
        status: "pending",
        createdAt: new Date(),
        updatedAt: new Date(),
      },   {
        order_id: "4884183d-1b88-461c-a028-9c5a8224d7af",
        user_id: "f479c1a0-cc9c-47c0-8594-24aa5557eef4",
        produk_id: "d159baa0-5fbc-4e14-b21f-1bada797d332",
        status: "pending",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
