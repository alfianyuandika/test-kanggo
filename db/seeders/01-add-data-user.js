"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("user", [
      {
        id: "4adc4e9e-fc6b-4644-b1e6-6b6a1433a9ac",
        name: "User1",
        email: "user1@gmail.com",
        password: "User1_12345",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "cf3efecf-2559-4c3b-b4e3-8d3100c8c931",
        name: "User2",
        email: "user2@gmail.com",
        password: "User2_12345",
        createdAt: new Date(),
        updatedAt: new Date(),
      },   {
        id: "f479c1a0-cc9c-47c0-8594-24aa5557eef4",
        name: "User3",
        email: "user3@gmail.com",
        password: "User3_12345",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },
  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
