'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("produk", [
      {
        id: "0ea2167b-66d9-4fd5-8adb-3d9202cfb69f",
        name: "Charger",
        price: 200000,
        qty: "5",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id: "2c425fe2-f62e-4f1a-a2e2-66869b081a92",
        name: "Kabel",
        price: 150000,
        qty: "5",
        createdAt: new Date(),
        updatedAt: new Date(),
      },   {
        id: "d159baa0-5fbc-4e14-b21f-1bada797d332",
        name: "Headphone",
        price: 350000,
        qty: "5",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
