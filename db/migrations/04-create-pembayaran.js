"use strict";
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.createTable("pembayaran", {
      id: {
        allowNull: false,
        primaryKey: true,
        unique: true,
        type: Sequelize.UUID,
      },
      order_id: {
        allowNull: false,
        type: Sequelize.UUID,
      },
      status: {
        allowNull: false,
        type: Sequelize.STRING,
        defaultValue: "paid",
      },
      amount: {
        allowNull: false,
        type: Sequelize.DECIMAL,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      deletedAt: {
        allowNull: true,
        type: Sequelize.DATE,
      },
    });
    await queryInterface.addConstraint("pembayaran", {
      fields: ["order_id"],
      type: "foreign key",
      name: "custom_fkey_order_id",
      references: {
        table: "transaksi",
        field: "order_id",
      },
      onDelete: "cascade",
      onUpdate: "cascade",
    });
  },
  down: async (queryInterface, Sequelize) => {
    await queryInterface.dropTable("pembayaran");
  },
};
