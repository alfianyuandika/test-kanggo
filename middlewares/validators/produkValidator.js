const { produk } = require("../../db/models");
const validator = require("validator");

exports.create = async (req, res, next) => {
  try {
    let errors = [];

    if (!validator.isNumeric(req.body.price)) {
      errors.push("Price must be a number");
    }

    if (!validator.isNumeric(req.body.qty)) {
      errors.push("Quantity must be a number");
    }

    if (errors.length > 0) {
      return res.status(400).json({
        message: errors.join(", "),
      });
    }

    next();
  } catch (e) {
    return res.status(500).json({
      message: "Internal Server Error",
      error: e,
    });
  }
};

exports.update = async (req, res, next) => {
  try {
    let errors = [];

    let findData = await produk.findOne({
      where: { id: req.params.id },
    });

    if (!findData) {
      errors.push("Produk Not Found");
    }
    if (!validator.isNumeric(req.body.price)) {
      errors.push("Price must be a number");
    }
    if (!validator.isNumeric(req.body.qty)) {
      errors.push("Quantity must be a number");
    }
    if (errors.length > 0) {
      return res.status(400).json({
        message: errors.join(", "),
      });
    }
   
    next();
  } catch (e) {
    return res.status(500).json({
      message: "Internal Server Error",
      error: e.message,
    });
  }
};

exports.delete = async (req, res, next) => {
  try {
    let findData = await produk.findOne({
      where: { id: req.params.id },
    });

    if (!findData) {
      return res.status(404).json({
        message: "Produk Not Found",
      });
    }

    next();
  } catch (e) {
    return res.status(500).json({
      message: "Internal Server Error",
      error: e.message,
    });
  }
};
